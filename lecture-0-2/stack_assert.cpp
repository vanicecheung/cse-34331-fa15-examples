// stack_default.cpp: normal stack

#include <cassert>
#include <stack>

const int NITEMS = 1<<25;

int main(int argc, char *argv[])
{
    std::stack<int> s;

    for (int i = 0; i < NITEMS; i++) {
    	s.push(i);
    }

    int i = NITEMS - 1;
    while (!s.empty()) {
    	assert(i == s.top());
    	s.pop();
    	i--;
    }

    return 0;
}
