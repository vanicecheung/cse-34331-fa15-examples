// linked_list_size.cpp: Singly Linked List (size)

#include <cstdlib>
#include <iostream>
#include <stdexcept>

const int NITEMS = 10;

// List declaration ------------------------------------------------------------

template <typename T>
class List {
    protected:
        typedef struct Node {
            struct Node *next;
            T            data;
        } Node;

        typedef Node * iterator;

        Node  *head;
        size_t length;	// Can't call it size...

    public:
        List() : head(NULL), length(0) {}	// Need to initialize it
        iterator front() { return head; };

        size_t size() const { return length; }	// Implement it inline
        T& at(const size_t i);
        void insert(iterator it, const T &data);
        void push_back(const T &data);
        void erase(iterator it);
};

// List implementation --------------------------------------------------------

template <typename T>
T& List<T>::at(const size_t i) {
    Node *node = head;
    size_t   n = 0;

    while (n < i && node != NULL) {
        node = node->next;
        n++;
    }

    if (node) {
        return node->data;
    } else {
        throw std::out_of_range("invalid list index");
    }
}

template <typename T>
void List<T>::insert(iterator it, const T& data) {
    if (head == NULL && it == NULL) {
        head = new Node{NULL, data};
    } else {
	it->next = new Node{it->next, data};
    }
    length++;	// Need to increment
}

template <typename T>
void List<T>::push_back(const T& data) {
    if (head == NULL) {
        head = new Node{NULL, data};
    } else {
	Node *curr = head;
	Node *tail = head;

	while (curr) {
	    tail = curr;
	    curr = curr->next;
	}

	tail->next = new Node{NULL, data};
    }
    length++;	// Need to increment
}

template <typename T>
void List<T>::erase(iterator it) {
    if (it == NULL) {
	throw std::out_of_range("invalid iterator");
    }

    if (head == it) {
	head = head->next;
	delete it;
    } else {
	Node *node = head;

	while (node != NULL && node->next != it) {
	    node = node->next;
	}

	if (node == NULL) {
	    throw std::out_of_range("invalid iterator");
	}

	node->next = it->next;
	delete it;
    }

    length--;	// Need to decrement
}

// Main execution -------------------------------------------------------------

int main(int argc, char *argv[]) {
    List<int> list;

    std::cout << "List Size: " << list.size() << std::endl;

    for (int i = 0; i < NITEMS; i++) {
        list.push_back(i);
    }

    std::cout << "List Size: " << list.size() << std::endl;
    std::cout << "List Items:" << std::endl;
    for (size_t i = 0; i < list.size(); i++) {
        std::cout << "List at " << i << " " << list.at(i) << std::endl;
    }

    auto head = list.front();
    list.insert(head, NITEMS + 1);
    list.insert(head, NITEMS + 2);
    list.insert(head->next->next, NITEMS + 3);

    std::cout << "List Size: " << list.size() << std::endl;
    std::cout << "List Items:" << std::endl;
    for (size_t i = 0; i < list.size(); i++) {
        std::cout << "List at " << i << " " << list.at(i) << std::endl;
    }

    list.erase(list.front());
    list.erase(list.front()->next);
    std::cout << "List Size: " << list.size() << std::endl;
    std::cout << "List Items:" << std::endl;
    for (size_t i = 0; i < list.size(); i++) {
        std::cout << "List at " << i << " " << list.at(i) << std::endl;
    }

    return 0;
}
