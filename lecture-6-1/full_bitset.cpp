#include <bitset>
#include <random>

using namespace std;

#define NUMBERS 1<<20

int main(int argc, char *argv[]) {
    bitset<NUMBERS> s;
    default_random_engine e;
    uniform_int_distribution<uint32_t> d(1, 6);

    for (size_t i = 0; i < NUMBERS; i++) {
    	if (d(e) != 6) {
	    s.set(i);
	}
    }
    
    for (size_t i = 0; i < NUMBERS; i++) {
    	s.test(i);
    }

    return 0;
}
