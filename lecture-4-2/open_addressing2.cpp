#include <cassert>
#include <exception>
#include <iostream>
#include <vector>

using namespace std;

#define TABLE_SIZE  (1<<10)
#define SENTINEL    0

template <typename T>
class OATable {
public:
    OATable(int size=TABLE_SIZE, int step=1) {
    	tsize = size;
    	ssize = step;
	table = vector<T>(tsize);
    }

    void insert(const T &value) {
    	int bucket = locate(value);
	table[bucket] = value;
    }

    bool find(const T &value) {
    	int bucket = locate(value);
	return table[bucket] == value;
    }

    void print() const {
    	for (int bucket = 0; bucket < tsize; bucket++) {
    	    cout << bucket << ": " << table[bucket] << endl;
	}
    }

private:
    int locate(const T &value) {
    	int bucket = value % tsize;
    	int step   = ssize;

    	while (table[bucket] != value && table[bucket] != SENTINEL && step < tsize) {
    	    bucket = (bucket + step) % tsize;
    	    step   = step + ssize;
	}

	if (step < tsize) {
	    return bucket;
	} else {
	    throw runtime_error("Could not find empty bucket");
	}
    }

    vector<T> table;
    int tsize;
    int ssize;
};

int main(int argc, char *argv[]) {
    OATable<int> s(10);

    if (argc != 2) {
    	cerr << "usage: " << argv[0] << " nitems" << endl;
    	return 1;
    }

    int nitems = atoi(argv[1]);

    for (int i = 1; i <= 2*nitems; i+=2) {
    	s.insert(i);
    }

    s.print();

    for (int i = 1; i <= 2*nitems; i+=2) {
    	assert(s.find(i));
    }

    return 0;
}
